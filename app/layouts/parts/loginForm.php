<?php if ($session->has('user')): ?>
    <p>Witaj, <?php echo $session->get('user') ?></p>

    <p><a href="<?php echo $router->generate('logout') ?>">Wyloguj</a></p>
<?php else: ?>
    <form action="<?php echo $router->generate('login_check') ?>" method="post">
        <input class="inline" name="username" value="" placeholder="Username" />
        <input class="inline" name="password" type="password" value="" placeholder="Password" />
        <br />
        <button class="button" type="submit">Zaloguj</button>
    </form>
<?php endif ?>