<?php

namespace App\Templating;

interface TemplatingEngine
{
    public function render(string $page, array $params = [], string $layout = 'default'): string;
}