<?php

namespace App\Form\Widget;

interface WidgetInterface
{
    public function render(string $name, string $id, $value, array $fieldOptions = []): string;
}