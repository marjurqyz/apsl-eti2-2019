<?php

namespace App\Form\Widget;

class InputWidget implements WidgetInterface
{
    const TYPE_TEXT = 'text';
    const TYPE_PASSWORD = 'password';
    const TYPE_NUMBER = 'number';

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $type
     */
    public function __construct(string $type = self::TYPE_TEXT)
    {
        $this->type = $type;
    }

    public function render(string $name, string $id, $value, array $fieldOptions = []): string
    {
        $html = <<<HTML
<input name="{$name}" type="{$this->type}" id="{$id}" value="${value}" />
HTML;

        return $html;
    }
}