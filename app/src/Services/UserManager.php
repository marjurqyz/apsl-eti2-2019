<?php

namespace App\Services;

use App\Exception\User\UserAlreadyExistsException;
use App\Model\UserCredentials;
use App\Repository\UserRepositoryInterface;
use App\Security\PasswordEncoderInterface;

class UserManager
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param UserRepositoryInterface $repository
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepositoryInterface $repository, PasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param string $username
     * @param string $plainPassword
     * @throws UserAlreadyExistsException
     */
    public function registerUser(string $username, string $plainPassword)
    {
        if ($this->isUserRegistered($username)) {
            throw UserAlreadyExistsException::withUsername($username);
        }

        $userCredentials = new UserCredentials($username, $this->passwordEncoder->encodePassword($plainPassword));
        $this->repository->saveUser($userCredentials);
    }

    /**
     * @param string $username
     * @return bool
     */
    public function isUserRegistered(string $username)
    {
        $userCredentials = $this->repository->findCredentialsByUsername($username);

        return $userCredentials instanceof UserCredentials;
    }
}