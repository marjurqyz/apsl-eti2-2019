<?php

namespace App\Security;

interface PasswordEncoderInterface
{
    public function encodePassword(string $plainPassword): string;
}