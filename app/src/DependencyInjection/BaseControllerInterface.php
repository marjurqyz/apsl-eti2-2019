<?php

namespace App\DependencyInjection;

use App\Router;
use App\Session\Session;

interface BaseControllerInterface
{
    public function injectRouter(Router $router);

    public function injectSession(Session $session);
}