<?php

namespace App\DependencyInjection;

interface Injector
{
    public function inject(ServiceContainer $container, $service);
}