<?php

namespace App\DependencyInjection;

class ControllerInjector implements Injector
{
    public function inject(ServiceContainer $container, $service)
    {
        if ($service instanceof TemplatingAwareInterface) {
            $service->injectTemplating($container->get('templating'));
        }

        if ($service instanceof BaseControllerInterface) {
            $service->injectRouter($container->get('router'));
            $service->injectSession($container->get('session'));
        }
    }
}