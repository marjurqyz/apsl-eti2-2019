<?php

namespace App\DependencyInjection;

use App\Templating\TemplatingEngine;

interface TemplatingAwareInterface
{
    public function injectTemplating(TemplatingEngine $templatingEngine);
}