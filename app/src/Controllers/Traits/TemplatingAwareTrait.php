<?php

namespace App\Controllers\Traits;

use App\Templating\TemplatingEngine;

trait TemplatingAwareTrait
{
    /**
     * @var TemplatingEngine
     */
    protected $templating;

    /**
     * @param TemplatingEngine $engine
     */
    public function injectTemplating(TemplatingEngine $engine)
    {
        $this->templating = $engine;
    }
}