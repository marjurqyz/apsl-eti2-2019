<?php

namespace App;

use App\DependencyInjection\ControllerInjector;
use App\DependencyInjection\ServiceContainer;
use App\Exception\PageNotFoundException;
use App\Exception\ServerErrorException;
use App\Response\ErrorResponse;

class App
{
    private $request;

    private $container;

    public function __construct()
    {
        $this->container = new ServiceContainer();
        $this->configure();
    }

    protected function configure()
    {
        $this->container->registerInjector(new ControllerInjector());
    }

    /**
     * Uruchamia aplikację
     * @throws \Exception
     */
    public function run(): void
    {
        $this->request = Request::fromGlobals();

        $router = $this->getRouter();
        try {
            $matchedRoute = $router->match($this->request);
            $response = $matchedRoute($this->request);
        } catch (PageNotFoundException $exception) {
            $response = new ErrorResponse($router, $exception, 404);
        } catch (ServerErrorException $exception) {
            $response = new ErrorResponse($router, $exception, 500);
        }

        foreach ($response->getHeaders() as $header) {
            header($header);
        }

        echo $response->getBody();
    }

    /**
     * @return Router
     * @throws \Exception
     */
    public function getRouter()
    {
        return $this->container->get('router');
    }
}