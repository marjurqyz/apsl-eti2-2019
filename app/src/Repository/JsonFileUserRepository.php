<?php

namespace App\Repository;

use App\Model\UserCredentials;
use App\Security\PasswordEncoderInterface;

class JsonFileUserRepository implements UserRepositoryInterface
{
    /**
     * @var array
     */
    private $users;

    /**
     * @var string
     */
    private $filename;

    /**
     * @param array $users
     */
    public function __construct(string $filename)
    {
        $this->users = [];
        $this->filename = $filename;
        foreach (json_decode(file_get_contents($filename), true) as $userData) {
            $this->users[$userData['username']] = $userData['password'];
        }
    }

    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        if (!isset($this->users[$username])) {
            return null;
        }

        return new UserCredentials($username, $this->users[$username]);
    }

    /**
     * @param UserCredentials $credentials
     */
    public function saveUser(UserCredentials $credentials): void
    {
        $this->users[$credentials->getUsername()] = $credentials->getPassword();
        $this->save();
    }

    private function save()
    {
        $data = [];
        foreach ($this->users as $username => $password) {
            $data[] = [
                'username' => $username,
                'password' => $password
            ];
        }

        file_put_contents($this->filename, json_encode($data));
    }
}