<?php

namespace App\Repository;

use App\Model\UserCredentials;

class ChainUserRepository implements UserRepositoryInterface
{
    /**
     * @var UserRepositoryInterface[]
     */
    private $repositories = [];

    /**
     * @var string
     */
    private $saveRepositoryName = null;

    /**
     * @param UserRepositoryInterface $repository
     */
    public function addRepository(string $name, UserRepositoryInterface $repository)
    {
        if (!$this->saveRepositoryName) {
            $this->saveRepositoryName = $name;
        }

        $this->repositories[$name] = $repository;
    }

    /**
     * @param string $name
     * @return string|null returns previous set repository
     * @throws \Exception
     */
    public function setSaveRepository(string $name): ?string
    {
        if (!isset($this->repositories[$name])) {
            throw new \Exception(sprintf('Repository "%s" does not exist', $name));
        }

        $previous = $this->saveRepositoryName;
        $this->saveRepositoryName = $name;

        return $previous;
    }

    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        foreach ($this->repositories as $repository) {
            $credentials = $repository->findCredentialsByUsername($username);

            if ($credentials instanceof UserCredentials) {
                return $credentials;
            }
        }

        return null;
    }

    /**
     * @param UserCredentials $credentials
     * @throws \Exception
     */
    public function saveUser(UserCredentials $credentials): void
    {
        if (!$this->saveRepositoryName) {
            throw new \Exception('No repository set for saving.');
        }

        $name = $this->saveRepositoryName;
        $this->repositories[$name]->saveUser($credentials);
    }
}